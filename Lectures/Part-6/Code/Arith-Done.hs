{- Simple Arithmetic Expressions
-}
data Exp
    = Const Int
    | Add Exp Exp
    | Mul Exp Exp
    | Div Exp Exp
    deriving Show

{- Examples
-}
x = Const 304
y = Const 7
z = Const 0
good = (Const 7 `Mul` (x `Add` z)) `Div` y
bad = (x `Add` y) `Div` z

{- Simple Evaluation
    is error prone
-}
sEval :: Exp -> Int
sEval e = case e of
    Const x -> x
    Add e1 e2 -> sEval e1 + sEval e2
    Mul e1 e2 -> sEval e1 * sEval e2
    Div e1 e2 -> sEval e1 `div` sEval e2

{- Error Handling Result Type
-}
data Result a
    = DivisionByZeroError
    | Success a
    deriving Show

{- Error Handling
    By hand, without abstractions.
-}
evalE :: Exp -> Result Int
evalE e = case e of
    Const x -> Success x
    Add e1 e2 -> case evalE e1 of
        DivisionByZeroError -> DivisionByZeroError
        Success x1 -> case evalE e2 of
            DivisionByZeroError -> DivisionByZeroError
            Success x2 -> Success $ x1 + x2
    Mul e1 e2 -> case evalE e1 of
        DivisionByZeroError -> DivisionByZeroError
        Success x1 -> case evalE e2 of
            DivisionByZeroError -> DivisionByZeroError
            Success x2 -> Success $ x1 * x2
    Div e1 e2 -> case evalE e1 of
        DivisionByZeroError -> DivisionByZeroError
        Success x1 -> case evalE e2 of
            DivisionByZeroError -> DivisionByZeroError
            Success x2
                | x2 /= 0 -> Success $ x1 `div` x2
                | otherwise -> DivisionByZeroError

{- Instances for Result
-}
instance Functor Result where
    fmap f (Success x) = Success $ f x
    fmap f _ = DivisionByZeroError

instance Applicative Result where
    Success f <*> Success x = Success $ f x
    _ <*> _ = DivisionByZeroError
    pure = Success

instance Monad Result where
    Success x >>= f = f x
    _ >>= _ = DivisionByZeroError

{- Error Handling
    with abstractions
-}
evalE2 :: Exp -> Result Int
evalE2 e = case e of
    Const x -> Success x
    Add e1 e2 -> (+) <$> evalE2 e1 <*> evalE2 e2
    Mul e1 e2 -> (*) <$> evalE2 e1 <*> evalE2 e2
    Div e1 e2 -> do
        x1 <- evalE2 e1
        x2 <- evalE2 e2
        if x2 == 0 then
            DivisionByZeroError
        else
            return $ x1 `div` x2



