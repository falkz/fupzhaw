{- Simple Arithmetic Expressions
-}
data Exp
    = Const Int
    | Add Exp Exp
    | Mul Exp Exp
    | Div Exp Exp
    deriving Show

{- Examples
-}
x = Const 304
y = Const 7
z = Const 0
good = (Const 7 `Mul` (x `Add` z)) `Div` y
bad = (x `Add` y) `Div` z

{- Simple Evaluation
    is error prone
-}
sEval :: Exp -> Int
sEval e = case e of
    Const x -> x
    Add e1 e2 -> sEval e1 + sEval e2
    Mul e1 e2 -> sEval e1 * sEval e2
    Div e1 e2 -> sEval e1 `div` sEval e2

{- Error Handling Result Type
-}
data Result a
    = DivisionByZeroError
    | Success a
    deriving Show

{- Exercise
    Rewrite the eval function so that errors are handled correctly.
    Write the function "by hand", without using abstractions (Monad, Applicative).
-}
evalE :: Exp -> Result Int
evalE e = error "fixme"

{- Exercise
    We want to refactor the code from before using abstactions of Functor,
    Applicative and Monad.
-}
{- Exercise
    First write instances for the Result type.
-}
instance Functor Result where
    -- (a - > b) -> f a -> f b
    fmap f (Success x) = Success (f x)
    fmap f _ = DivisionByZeroError

instance Applicative Result where
    -- f (a -> b) -> f a -> f b
    Success f <*> Success x = error "fixme"
    -- pure :: a -> Result a
    pure = error "fixme"

instance Monad Result where
    -- m a -> ( a -> m b) -> m b
    Success x >>= f = error "fixme"
    _ >>= _ = error "fixme"

{- Exercise
    Now use the instances to refactor the eval function.
-}
evalE2 :: Exp -> Result Int
evalE2 e = case e of
    Const x -> error "fixme"
    Add e1 e2 -> error "fixme"
    Mul e1 e2 -> error "fixme"
    Div e1 e2 -> error "fixme"