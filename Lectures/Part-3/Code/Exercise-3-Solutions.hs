--------------------------------------------------------------------------------
-- Exercise 1: Filesystem
--------------------------------------------------------------------------------

{-
 Gegeben ist ein einfacher Datentyp für Verzeichnisstrukturen (Ordnerbäume).
 Eine Verzeichnisstruktur besteht in diesem Modell entweder aus einem
 einzelnen File (durch seinen Namen und Grösse gegeben), oder durch eine Liste
 von Unterverzeichnissen.
-}
type Name = String
type FileSizeKB = Int

data FileSystem
    = File Name FileSizeKB
    | Dir Name [FileSystem]
    deriving Show

example :: FileSystem
example = Dir "root"
    [ Dir "desktop"
        [ File "notes.txt" 87
        , File "brochure.pdf" 581
        ]
    , Dir "pictures"
        [ Dir "holiday2019"
            [ File "paris1.png" 2075
            , File "paris2.png" 3017
            ]
        , Dir "holiday2018"
            [ File "rome1.jpg" 2075
            , File "rome2.jpg" 4584
            , File "rome3.png" 2075
            , File "notes.txt" 112
            ]
        ]
    ]


{- Aufgabe:
    implementieren Sie eine "allgemeine Fold Funktion" wie in der Vorlesung
    besprochen.
-}
filesystem :: (Name -> FileSizeKB -> b) -> (Name -> [b] -> b) -> FileSystem -> b
filesystem file dir fs = case fs of
    File n s -> file n s
    Dir n subs -> dir n $ map (filesystem file dir) subs

{-
    Ausgehend von der Funktion 'filesystem' wollen wir nun konkrete Funktionen
    implementieren, die uns im Umgang mit dem 'FileSystem' Typ nützlich
    erscheinen.
-}

{- Grösse eines Verzeichnisses
    Implementieren Sie die Funktion 'size', die die grösse einer gegebenen
    Verzeichnisstruktur zurück gibt. Verwenden Sie die Funktion 'filesystem'.
-}
size :: FileSystem -> Int
size = filesystem (\_ s -> s) (\_ xs -> sum xs)

{- Datei abfragen
    Implementieren Sie die Funktion 'existsFile', die bei einer gegebenen
    Verzeichnisstruktur zurück gibt, ob darin eine Datei mit dem mitgegebenen
    Namen zu finden ist. Verwenden Sie die Funktion 'filesystem'.
-}
existsFile :: Name -> FileSystem -> Bool
existsFile name = filesystem file dir
    where
        file n _ = n == name
        dir _ = or

{- Pfade
     Ein Pfad ist eine Liste von Namen.
     Beispiel: "/pictures/holiday2019/paris1.png" wäre
                ["pictures", "holiday2019", "paris1.png"]
-}
type Path = [Name]

{- Alle Pfade finden
    Implementieren Sie die Funktion 'findAll' mit folgendem Verhalten:
    Input: Dateiname, Verzeichnisstruktur
    Rückgabe: Alle Pfade in der gegebenen Struktur, die auf eine Datei mit dem
              gegebenen Namen zeigen.
    Verwenden Sie auch hier die Funktion 'filesystem'.
-}
findAll :: Name -> FileSystem -> [Path]
findAll name = filesystem file dir
    where
        file :: Name -> Int -> [Path]
        file n _ | n == name = [[n]]
                 | otherwise = []
        dir :: Name -> [[Path]] -> [Path]
        dir n subs = map ((:) n) $ concat subs


--------------------------------------------------------------------------------
-- Exercise 2: Regular Expressions
--------------------------------------------------------------------------------

-- Abstract syntax tree for regular expressions.
data Regex
    = Empty
    | Epsilon
    | Symbol Char
    | Sequence Regex Regex
    | Star Regex
    | Choice Regex Regex
    deriving Show

{-
    We want a function that given a regex computes a list of all strings matching
    this regex. However, if the regex contains the 'Star' constructor, then the
    resutling list will be infinite. Therefore, we add a counter limiting the
    number of repetitions allowed by the star operator. When checking if a string
    matches the regex, we can use the length of the string as limit.
-}
generate :: Int -> Regex -> [String]
generate limit r = case r of
    Empty -> []
    Epsilon -> [""]
    Symbol c -> [[c]]
    Choice r s -> generate limit r ++ generate limit s
    Sequence r s -> [x++y | x <- generate limit r, y <- generate limit s]
    Star r -> concat [times n r | n <- [0..limit]]
    where
        times :: Int -> Regex -> [String]
        times 0 r = [""]
        times n r =  [x++y | x <- generate limit r, y <- times (n-1) r]

{-
    Matching a particular string to a regex a regex now simply means to check
    whether the string is an element of the list of all matching strings.
    The string's length is a rough upper bound on the number of repetitions
    for any application of the 'Star' operator.
-}
match :: Regex -> String -> Bool
match r s = s `elem` generate (length s) r

{-
    Examples/test-cases
-}

-- r = a(a|b)*
r :: Regex
r = Sequence (Symbol 'a') $ Star $ Choice (Symbol 'a') (Symbol 'b')

shouldBeTrue_r :: Bool
shouldBeTrue_r = match r "abababa"

shouldBeFalse_r :: Bool
shouldBeFalse_r = match r ""

-- s = x*a*b*
s :: Regex
s = Sequence (Star (Symbol 'x')) $ Sequence (Star (Symbol 'a')) (Star (Symbol 'b'))

shouldBeTrue_s :: Bool
shouldBeTrue_s = match s "xxxxxaaab"

shouldBeFalse_s :: Bool
shouldBeFalse_s = match r "aabbx"