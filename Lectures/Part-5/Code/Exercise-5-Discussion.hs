import Data.Functor.Contravariant (Contravariant (contramap))
newtype Boxed a = Boxed { unbox :: a }

instance Functor Boxed where
    fmap f (Boxed a) = Boxed $ f a
    -- f <$> (Boxed a) = Boxed (f a)
{-

law1: id <$> (Boxed a) = Boxed (id a) = Boxed a

law2: (f . g) <$> (Boxed a)
    = Boxed ((f . g) a)
    = Boxed ((f (g a))  // b = (g a)
    = Boxed (f b)
    = f <$> (Boxed b)
    = f <$> (Boxed (g a))
    = f <$> (g <$> (Boxed a))
-}


newtype FromInt a = FromInt {fun :: Int -> a }

instance Functor FromInt where
    -- fmap :: (a -> b) -> FI (Int -> a) -> FI (Int -> b)
    fmap f (FromInt g) = FromInt $ f . g

{-

law1: id <$> (FI f) = FI (id . f) = FI f

law2: (f . g) <$> (FI h)
    = FI ((f.g).h)
    = FI (f.(g.h))
    = f <$> FI (g.h)
    = f <$> (g <$> (FI h))
-}


newtype ToInt a = ToInt { code :: a -> Int }

instance Contravariant ToInt where
    contramap f (ToInt g) = ToInt $ g . f


{-
contramap :: ( b -> a ) -> f a -> f b
contramap :: ( b -> a ) -> TI (a -> Int) -> TI (b -> Int)
             f                g
g .  f :: b -> Int
-}