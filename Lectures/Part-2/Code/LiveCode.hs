

fromInt :: Integer -> Nat
fromInt 0 = Z 
fromInt n = S $ fromInt (n-1)

fromNat :: Nat -> Integer
fromNat Z = 0
fromNat (S n) = (fromNat n) + 1


data Nat 
    = Z 
    | S Nat
    deriving (Show, Eq)

add :: Nat -> Nat -> Nat
add n Z = n
add n (S m) = add (S n) m

mul :: Nat -> Nat -> Nat
mul n Z = Z
mul n (S m) = add (mul n m) n 

fact :: Nat -> Nat
fact Z = S Z
fact (S n) = mul (S n) (fact n)

--- Fractran

data Fraction = Fraction
    { numerator :: Integer
    , denominator :: Integer
    } deriving Show

instance Eq Fraction where
    Fraction x y == Fraction a b = x * b == a * y

make :: Integer -> Integer -> Fraction
make a b = Fraction (a `div` g) (b `div` g)
    where
        g = gcd a b

type Program = [Fraction]

type Input = Integer

type Output = [Integer]

execute :: Program -> Input -> Output
execute p i = reverse $ run p [] i 
    where
        run :: Program -> Output -> Input -> Output
        run state acc input = case state of
            (Fraction a b):xs  
                | input `mod` b == 0 -> run p (input:acc) (input*a `div` b)
                | otherwise -> run xs acc input 
            [] -> input:acc

p :: Program
p = [ make 5 3
    , make 2 5
    ]

fibProgram :: Program
fibProgram =
    [ make 91 33
    , make 11 13
    , make 1 11
    , make 399 34
    , make 17 19
    , make 1 17
    , make 2 7
    , make 187 5
    , make 1 3
    ]
