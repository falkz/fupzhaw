
data Color = RGB Integer Integer Integer
    deriving (Show, Eq, Ord)

type Make = String

data Power
    = PS Integer
    deriving (Show, Eq, Ord)
    -- | KW Integer

newtype Model = Model String
    deriving (Show, Eq, Ord)

data Car = Car
    { model :: Model
    , make :: Make
    , year :: Integer
    , color :: Color
    , power :: Power
    } deriving (Show, Eq, Ord)


{-
ford Einen roten Ford Fiesta mit 70P S aus dem Jahr 2017.
ferrari Einen grünen Ferrari (andere Parameter frei).
zoe Einen weissen Renault Zoe (andere Parameter frei)
-}

ferrari :: Car
ferrari = Car
    (Model "Testarossa")
    "Ferrari"
    1985
    (RGB 0 255 0)
    (PS 400)

ford :: Car
ford = Car
    { model = Model "Fiesta"
    , make = "Ford"
    , year = 2017
    , color = RGB 255 0 0
    , power = PS 70
    }


data Tree a = Leaf a | Node (Tree a) a (Tree a)

collect :: Tree a -> [a]
collect t = case t of
    Leaf a -> [a]
    Node l x r -> x:((collect l) ++ (collect r))

{-
    1
   / \
  2  3
-}
t123 :: Tree Integer
t123 = Node (Leaf 2) 1 (Leaf 3)


data MTree a = MTree a [MTree a]

exampleTree :: MTree Integer
exampleTree =
  MTree 1
    [ MTree 2
        [ MTree 5 []
        , MTree 6 []
        , MTree 7 []
        ]
    , MTree 3
        [MTree 8 []]
    ]