\documentclass[xcolor=dvipsnames
    %, handout
    ]{beamer}

\input{../Auxiliary-latex-files/header.tex}

\title{Funktionale Programmierung\\
	\small{Funktionen und Typen I} }
\date{}

\def\tabularxcolumn#1{m{#1}}
\setbeamertemplate{footline}[frame number]


\begin{document}

\maketitle

\section*{Lektion 2}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	\tableofcontents
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Referenzielle Transparenz}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Werte und Variablen}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
In der funktionalen Programmierung haben ``Variablen'', und
damit Wertzuweisungen eine \textbf{fundamental} andere
Bedeutung als in der imperativen Programmierung.
\vspace{0.5cm}
\pause

Die Zuweisung \texttt{x = 3} im Vergleich:
\pause
\begin{itemize}[<+->]
	\item Funktional: Der ``Name'' \texttt{x} benennt
	(in seinem Kontext), unabhängig von der Zeit, den
	\textbf{Wert} 3.
	\item Imperativ: Der ``Name'' \texttt{x} benennt einen
	\textbf{Ort} (Speicherbereich). Sein Wert ändert sich
	mit der Zeit, je nachdem was in besagtem Speicherbereich
	steht.
\end{itemize}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	Konsequenzen:
	\begin{itemize}[<+->]
		\item Die Wert-Variable-Relation ist im funktionalen
		Paradigma zeitunabhängig.
		\item Variablen im funktionalen Paradigma
		entsprechen eher ``Konstanten'' als Variablen $ \to$
		Stichwort ``immutability''.
	\end{itemize}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Nebeneffekte}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	Neben dem Verzicht Variablen zu verändern, wird in der
	funktionalen Programmierung darauf geachtet auch andere
	Nebeneffekte möglichst zu vermeiden oder mindestens zu
	isolieren.
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}[fragile]
``Interne'' Nebeneffekte ändern den Zustand des Programms und kommen in rein funktionalen Sprachen nicht vor.
\begin{lstlisting}[frame=leftline, language = none]
int square(int x){
    if (cntr < 3) {
        cntr++;
        return x * x;
        };
    else failwith("enough is enough");
}
\end{lstlisting}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}[fragile]
	``Externe'' Nebeneffekte verändern den Zustand des Kontextes (Aussenwelt)  in den das Programm eingebettet ist und werden vom Rest des Programmes isoliert.
\begin{lstlisting}[frame=leftline, language = none]
f n = "Blink LED n times"
\end{lstlisting}
oder
\begin{lstlisting}[frame=leftline, language = none]
greet name = printLn "Hello" ++ name
\end{lstlisting}
Solche Nebeneffekte werden in funktionalen Sprachen möglichst gut isoliert und explizit gekennzeichnet.
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Referenzielle Transparenz}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	Eigenschaften von Nebeneffektfreiem (reinem) funktionalen Code:

	\begin{itemize}
		\item Eine ``Variable'' kann in einem Ausdruck immer\footnote{Unter Berücksichtigung ihres Kontextes!} durch ihren Wert ersetz werden. Der Wert (Bedeutung) des betreffenden Ausdrucks (Programmes)  verändert sich dadurch nicht.
		\item Der Wert eines Ausdrucks hängt nur von den Werten seiner Teilausdrücke ab.
		\item Die Evaluation eines Ausdruckes ist unabhängig von Reihenfolge, in der
		seine
		Teilausdrücke evaluiert werden.
	\end{itemize}

	Diese Eigenschaften werden unter dem Begriff der referenziellen Transparenz
	zusammengefasst.
	Imperativer Code ist (im allgemeinen) nicht referenziell transparent.
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	Vorteile, die sich aus referenzieller Transparenz ergeben:
	\begin{itemize}
		\item Mehr Flexibilit\"at beim Auswerten von Ausdrücken (z.B. ``lazy
		evaluation'').
		\item Einfachere Programmverifikation (weniger und einfachere/explizitere Abhängigkeiten).
		\item Erleichtert die Beweisführung, z.B. dass gewisse Optimierungen die Bedeutung eines transformierten Programmes nicht verändern.
		\item Erleichtert es Programme zu verstehen und zu entwerfen (``equational reasoning'').
	\end{itemize}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Typen}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	\begin{center}
		\includegraphics[width=0.5\textwidth]{../Auxiliary-latex-files/Images/venn.png}
	\end{center}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Was sind Typen}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	Darüber was genau ein Typ sei gibt es viele\footnote{Offenbar mindestens fünf} Meinungen:

	\begin{quote}
		D. L. Parnas, J. E. Shore and David Weiss identified five definitions of a
		``type'' that were used - sometimes implicitly - in the literature:\dots\\
		\color{BrickRed}{Wikipedia}
	\end{quote}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	Einige Meinungen sind etwas ausführlicher:
	\pause
	\begin{quote}
		A type system is a tractable syntactic method of proving the
		absence of certain program behaviors by classifying phrases
		according to the kinds of values they compute.\\
		\color{BrickRed}{Benjamin Pierce, Types and Programming Languages}
	\end{quote}
	\pause
	Wir wollen für den Moment mit einer einfachen Analogie arbeiten:
	\begin{itemize}
		\item Typen $\simeq$ Mengen
		\item Anstelle von $x\in t$ schreibt man $x:t$ ($x::t$ in
		Haskell)
		\item Sie dienen dazu eine ``minimale Konsistenz'' von Programmen zur Kompilierzeit sicherzustellen.
	\end{itemize}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	Typensysteme werden meistens nach folgendem Schema spezifiziert:
	\begin{itemize}
		\item Angabe einer Menge von ``primitiven''- oder ``Grundtypen'' (z.B. \texttt{Int}, \texttt{Char}, \texttt{Float}, etc.)
		\item Syntaktische Elemente, die es erlauben aus bestehenden Typen neue Typen zu bauen.
		\item Algorithmen, die überprüfen ob ein Programm zu einem gegebenen Typ passt (type-checker) oder sogar einen passenden Typ herleiten (type-inference).
	\end{itemize}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Grundtypen in Haskell}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Eine Auswahl an Grundtypen (String und Bool sind streng genommen keine Grundtypen sondern Zusammengesetzt) in Haskell:
    \begin{itemize}
        \item Der \texttt{Bool} Typ nimmt zwei mögliche Werte an, \texttt{True} oder
        \texttt{False}.
        \item Der \texttt{Char} Typ beinhaltet Zeichen wie \texttt{'a','B','/n'} etc.
        \item Der String Typ beinhaltet sequenzen von \texttt{Char} z.B. \texttt{"abc"}.
        Strings in Haskell sind Listen von Chars (und nicht sehr effizient).
        \item der \texttt{Int} Typ steht für ganze Zahlen in einem Bereich der Platformabhängig ist. Testen Sie den Bereich mit \texttt{maxBound :: Int} und \texttt{minBound :: Int} in der Repl.
        \item \texttt{Integer} beinhaltet beliebige ganze Zahlen.
    \end{itemize}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Zusammmengesetzte Typen - Listentypen}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Zu jedem gegebenen Typ \texttt{a} gibt es einen Typ \texttt{[a]} der alle Listen
    beinhaltet, die Eintr\"age vom Typ \texttt{a} beinhalten.
    \begin{bsp}
        \begin{itemize}
            \item \texttt{["abc", "xyz"]} ist vom Typ \texttt{[String]}
            \item \texttt{[1,2,3]} ist vom Typ \texttt{[Integer]}
            \item \texttt{[['a']]} ist vom Typ \texttt{[[Char]]}
        \end{itemize}
    \end{bsp}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Zusammengesetzte Typen - Funktionstypen}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Für alle Typen \texttt{a} und \texttt{b} beinhaltet der Typ \texttt{a->b} alle
    Funktionen, die eine Eingabe vom Typ \texttt{a} akzeptieren und eine Ausgabe vom Typ
    \texttt{b} zurückgeben.
    \begin{bsp}
        Der Typ \texttt{String -> Bool} beinhaltet Funktionen, die einen String
        akzeptieren und jeweils \texttt{True} oder \texttt{False} zurückgeben.
    \end{bsp}
    \begin{rk}
        \begin{itemize}
            \item Funktionstypen lassen sich beliebig ``verschachteln'', wir werden
            sp\"ater noch genauer auf diesen Umstand eingehen.
            \item Eine Funktion vom Typ \texttt{a->Bool} nennt man ein ``Pr\"adikat'' auf
        \texttt{a} (entspricht einer
        Eigenschaft).
        \end{itemize}
    \end{rk}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Zusammengesetzte Typen - Tupel}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Für alle Typen \texttt{a1}...\texttt{an} beinhaltet der Typ \texttt{(a,..,an)} alle
    Tupel, bei denen der $i$-te Eintrag vom Typ \texttt{ai} ist.
    \begin{bsp}
        Der Typ \texttt{(Int, String, Char)} beinhaltet das Tripel $(1,"1",'1')$.
    \end{bsp}
    \begin{rk}
        \begin{itemize}
            \item Tupel lassen sich beliebig ``verschachteln''.
        \end{itemize}
    \end{rk}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Zusammengesetzte Typen - Records}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
    Records sind Tupel, in denen die Einträge Labels (Namen) tragen.
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}[fragile]
    Beispiel eines Records:
    \begin{lstlisting}
data Customer = Customer
    { customerId :: Integer
    , name :: String
    }
    \end{lstlisting}
    Records definieren automatisch Funktionen um auf ihre Datenfelder zuzugreifen:
    \begin{lstlisting}
customerId :: Customer -> Integer
name :: Customer -> String
    \end{lstlisting}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Zusammengesetzte Typen - Summen}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}[fragile]
	In der Mengenanalogie entspricht der Summentyp der Vereinigung von disjunkten
	Mengen\footnote{Ein wesentlicher Unterschied besteht darin, dass den Elementen des
	Summentyps die Zugehörigkeit zum entsprechenden ``Summanden'' explizit mitgegeben
	wird.}. In Haskell können Summen direkt deklariert werden:
\begin{lstlisting}
data Shape
    = Rectangle Float Float
    | Square Float
    | Circle Float
\end{lstlisting}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}[fragile]
	Summentypen lassen sich direkt mit Patterns dekonstruieren:
\begin{lstlisting}
area :: Shape -> Float
area (Rectangle a b) = a * b
area (Square a) = a * a
area (Circle r) = 3.14 * r * r
\end{lstlisting}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}[fragile]
	Summentypen können auch rekursiv sein:
\begin{lstlisting}
data Tree a
    = Node (Tree a) a (Tree a)
    | Leaf a
\end{lstlisting}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}[fragile]
Rekursive Summen lassen sich mit Rekursion und Patterns dekonstruieren:
\vspace{0.5cm}

\begin{lstlisting}
-- Tiefe eines Baumes
depth :: Tree a -> Integer
depth (Node left _payload right) =
    1 + max (depth left) (depth right)
depth (Leaf _payload) = 1
\end{lstlisting}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}[fragile]
    \begin{ex}
        Geben Sie den Term an, der dem folgenden ``\texttt{Tree Integer}'' entspricht:
        \begin{center}
            \synttree[5 [2] [1 [2][3]]]
        \end{center}
    \end{ex}
    \begin{ex}
        Zeichnen Sie den zum folgenden Term passenden Baum:
\begin{lstlisting}
Node (Leaf 1) 4 (Node (Leaf 4) 0 (Leaf 9))
\end{lstlisting}
    \end{ex}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	\begin{ex}
		Die Karten eines Spieles (z.B.) Black-Jack bezeichnen entweder
		\begin{itemize}
			\item einen König (King),
			\item eine Dame (Queen),
			\item einen Buben (Jack),
			\item ein Ass (Ace)
			\item oder einen numerischen Wert.
		\end{itemize}
	Implementieren Sie einen Summentyp der diese Tatsache reflektieret.
	\end{ex}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	\begin{ex}
		Die Werte der Karten (z.B. in Black-Jack) seien durch folgende Zuordnung gegeben:
		\begin{itemize}
		\item Figuren (King, Queen, Jack) haben den Wert $10$
		\item Karten mit numerischem Wert haben ebendiesen Wert
		\item Das Ass hat entweder den Wert $1$ oder den Wert $11$
		\end{itemize}
	Implementieren Sie eine Funktion \texttt{value}, die den Wert einer Karte berechnet
	sowie einen Typ der Kartenwerte repr\"asentiert.
	\end{ex}
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}[fragile]
	Listen\footnote{Einfach verkettete Listen} sind als Summentyp definiert:
\begin{lstlisting}
data List a
    = Cons a (List a)
    | Nil
\end{lstlisting}
	Wie wir bereits wissen, stellt Haskell, so wie auch viele funktionale Sprachen, für Listen einiges an ``syntactic sugar'', sowie eine umfangreiche Bibliothek zur Verfügung.
\end{fr}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{fr}
	\begin{ex}
		Studieren Sie das Listen Modul (\url{https://hackage.haskell.org/package/base-4.14.1.0/docs/Data-List.html}) und testen Sie verschiedene Funktionen.
    \end{ex}
\end{fr}

\subsection{Typklassen}

\begin{fr}
    In einer Typklasse werden verschiedene Typen, die bestimmte Eigenschaften teilen
    zusammengefasst (\"ahnlich einem Interface).

    \begin{bsp}
        Die Typklasse \texttt{Eq} enth\"alt alle Typen deren Elemente vergleichbar sind.
        Dies wird deutlich, wenn wir die Signatur der Funktion \texttt{(==)} betrachten
        \begin{align*}
        \texttt{(==) :: Eq a => a -> a -> Bool}
        \end{align*}
    \end{bsp}

\end{fr}

\begin{fr}
    Indem wir diesen die in einer Typklasse festgelegten Funktionen auf einem Typ
    definieren, können wir diesen der entsprechenden Klasse hinzufügen (den Typ zu
    einer Instanz machen). Einige Instanzen lassen sich auch automatisch ableiten, dies
    wird syntaktisch mit dem \texttt{deriving} Keyword gemacht.
\end{fr}

\begin{fr}[fragile]
    \begin{bsp}
        \begin{lstlisting}
data Person = Person
    { name :: String
    , age :: Integer
    , idNumber :: Integer
    }

instance Eq Person where
  (==) person1 person2 =
    name person1 == name person2
    && age person1 == age person2
    && idNumber person1 == idNumber person2
        \end{lstlisting}
    \end{bsp}
\end{fr}

\begin{fr}
    Viele Typklassen verlangen, dass ihre Instanzen (respektive deren Implementierung)
    gewisse Eigenschaften erfüllen, die nicht vom Compiler überprüft werden
    können (Unter Anderem soll \texttt{==} beispielsweise eine \"Aquivalenzrelation
    sein).

    Wir werden sp\"ater konkrete Typklassen (und ihre Regeln) im Detail anschauen.
\end{fr}

\end{document}
