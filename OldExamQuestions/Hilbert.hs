type Name = String
type Year = Int

data Descendant = Descendant Name Year [Descendant]

hilbert :: Descendant 
hilbert = 
    Descendant "David Hilbert" 1885
        [ Descendant "Wilhelm Ackermann" 1925 []
        , Descendant "Haskell Curry" 1930 
            [ Descendant "Bruce Lercher" 1963 []
            ]
        , Descendant "Anne Bosworth" 1899 []
        ]


countDescendants :: Descendant -> Int
countDescendants (Descendant _ _ xs) =
    length xs + (sum (map countDescendants xs))

check :: Descendant -> Bool
check descendant = case descendant of
    Descendant _ _ [] -> True
    Descendant n year (x:xs) ->
        year < getYear x 
        && check x 
        && check (Descendant n year xs)
    where
        getYear :: Descendant -> Year
        getYear (Descendant _ y _) = y 
