# References and Links

## Course Webiste

-  [Moodle](www.fixme.ch)

## Functional Programming General Links

- [Wikipedia](https://en.wikipedia.org/wiki/Functional_programming)
- [A Brief History of Functional Programming](http://www.cse.psu.edu/~gxt29//historyOfFP/historyOfFP.html#:~:text=the%20value%20twice!-,Early%20Functional%20Languages,the%20functional%20programming%20paradigm%20significantly.)
- [Why Functional Programming Matters (by John Hughes)](https://www.cs.kent.ac.uk/people/staff/dat/miranda/whyfp90.pdf)
- [Functional Programming For The Rest of Us (blog post)](https://www.defmacro.org/2006/06/19/fp.html#history)


## Haskell Links

- [Documentation](https://www.haskell.org/documentation/) with many more links (Books, Courses, Tutorials, etc.).
- [The Haskell tool stack](https://docs.haskellstack.org/en/stable/README/)
- [Hackage](https://hackage.haskell.org/), the Haskell package repository
- [Hoogle](https://hoogle.haskell.org/), a Haskell API search engine, which allows you to search the Haskell libraries on Stackage by either function name, or by approximate type signature.